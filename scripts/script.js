/**
 * Created by bak on 18.05.17.
 */
$(document).ready(init)
server_domain = "http://localhost:8000";
function init() {
    var zalogowany = localStorage.getItem('wybory_auth');
    zalogowany = JSON.parse(zalogowany);
    
    if (zalogowany == false) {
      $("#login_button").html('<a href="login.html">Login</a>');
    } else {
      $("#login_button").html('<a href="logout.html">Logout</a>');
    }

    parseURL();
}

function parseURL() {
    var search = window.location.search;


    console.log(window.location.pathname)

    if (!search) {
        buildWybory("http://localhost:8000/api/wybory/", false);
        $("#tytul").html("Wybory");
    }
    else if (getParameterByName('wojewodztwo')) {
        par = getParameterByName('wojewodztwo');
        buildWybory(server_domain + "/api/wybory/wojewodztwo/" + par, false);
        $("#tytul").html("Wojewodztwo");
    }
    else if (getParameterByName('okreg')) {
        par = getParameterByName('okreg');
        buildWybory(server_domain + "/api/wybory/okreg/" + par, false);
        $("#tytul").html("Okreg");
    }
    else if (getParameterByName('gmina')) {
        par = getParameterByName('gmina');
        buildWybory(server_domain + "/api/wybory/gmina/" + par, false);
        $("#tytul").html("Gmina");
    }
    else if (getParameterByName('obwod')) {
        var zalogowany = localStorage.getItem('wybory_auth');
        zalogowany = JSON.parse(zalogowany);
        par = getParameterByName('obwod');
        buildWybory(server_domain + "/api/wybory/obwod/" + par, zalogowany);
        $("#tytul").html("Obwod");
    }
    else {
        buildWybory("http://localhost:8000/api/wybory/", false);
        $("#tytul").html("Wybory");
    }
}

function buildWybory(url, edit) {
	/*
    var wyniki = localStorage.getItem(url);
    
    if (url.indexOf("/api/wybory/obwod/") == -1 && wyniki) {
        console.log("Z CACHE laduje");
        renderLoadedData(JSON.parse(wyniki), edit); 
        return;
    }*/

    console.log("Pobieram dane z serwera");

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            wyniki = result;
            if (url.indexOf("/api/wybory/obwod/") == -1) {
              localStorage.setItem(url, JSON.stringify(wyniki));  
            }
            
            renderLoadedData(wyniki, edit);
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function fillResultTableEdit(wyniki_kandydatow) {
    var i;
    console.log(wyniki_kandydatow[0]);

    var html = '<tr><th>Kandydaci</trth><th>Zdobyte głosy</th><th></th></tr>';
    for (i = 0; i < wyniki_kandydatow.length; i++) {
      button = '<input type="button" onclick="location.href=' + "'edit.html?obwod=" + getParameterByName('obwod') +
      "&kandydat=" + wyniki_kandydatow[i].id + "'" + '" value="Edit" />';

        html += '<tr>' +
                '<td>' + wyniki_kandydatow[i].nazwa +'</td>' +
                '<td>' + wyniki_kandydatow[i].glosy + '</td>' +
                '<td>' + button + '</td>'
                '</tr>';
    }
    $("#result_table").html(html);
}

function fillResultTable(wyniki_kandydatow) {
    var i;
    var html = '<tr><th>Kandydaci</trth><th>Zdobyte głosy</th></tr>';
    for (i = 0; i < wyniki_kandydatow.length; i++) {
        html += '<tr>' +
                '<td>' + wyniki_kandydatow[i].nazwa +'</td>' +
                '<td>' + wyniki_kandydatow[i].glosy + '</td>' +
                '</tr>';
    }

    $("#result_table").html(html);
}

function fillStatisticsTable(statistics, zakres) {
    var html = '' +
            '<tr><th>Statystyki</th><th>Ilość</th></tr>' +
            '<tr><td>Karty wydane</td><td>'+
            statistics.karty_wydane + '</td></tr>' +
            '<tr> <td>Uprawnieni</td><td>' +
            statistics.uprawnieni + '</td></tr>' +
            '<tr><td>Głosy oddane</td><td>' +
            statistics.glosy_oddane + '</td></tr>' +
            '<tr><td>Głosy ważne</td><td>' +
            statistics.glosy_wazne + '</td></tr>' +
            '<tr><td>Głosy nieważne</td><td>' +
            statistics.glosy_niewazne + '</td></tr>';
           
    if (zakres !== 'obwod'){
    	html += '<tr> <td>Obwodów</td><td>' +
            statistics.obwodow + '</td></tr>'
    }

    $("#statistics_table").html(html);
}

function fillListElements(elements, zakres) {
    var query = '';
    var html = '<ul class="items">';
    if (zakres === 'wojewodztwo') {
        $("#list_label").html('<hr><h3>Lista okręgów:</h3>');
        query = 'okreg';
        for (i = 0; i < elements.length; i++) {
	        html += '<a href="wybory.html?' + query +'=' + elements[i]['id'] + '">' +
	            '<li class="item">' + '(' +elements[i]['id'] + ')</li></a>';
    	}
    }
    else if (zakres === 'okreg') {
        $("#list_label").html('<hr><h3>Lista gmin:</h3>');
        query = 'gmina';
        for (i = 0; i < elements.length; i++) {
	        html += '<a href="wybory.html?' + query +'=' + elements[i]['id'] + '">' +
	            '<li class="item">' + '(' +elements[i]['id'] + ')' + ' ' + elements[i]['nazwa_gminy'] +' </li></a>';
    	}
    }
    else if (zakres === 'gmina') {
        $("#list_label").html('<hr><h3>Lista obwodów:</h3>');
        query = 'obwod';
        for (i = 0; i < elements.length; i++) {
	        html += '<a href="wybory.html?' + query +'=' + elements[i]['id'] + '">' +
	            '<li class="item">' + '(' +elements[i]['id'] + ')' + ' ' + elements[i]['adres'] +' </li></a>';
    	}
    }
    else if (zakres === 'kraj') {
        $("#list_label").html('<hr><h3>Lista wojewodztw:</h3>');
        query = 'wojewodztwo';
        elements = localStorage.getItem("wojewodztwa");
        elements = JSON.parse(elements)
        for (var key in elements) {
        	if (elements.hasOwnProperty(key)) {
        		html += '<a href="wybory.html?' + query +'=' + key + '">' +
            		'<li class="item">' + '(' +key + ')</li></a>';
        	}

    	}

    }
    else {
        $("#list_label").hide();
        return;
    }

    var i;

    console.log(elements);
    if(elements.length === 0) {
        $("#list_elements").hide();
        return;
    }

//<a href="/wybory/okreg/{{ obiekt.obwod__gmina__okreg__id }}"><li class="item">{{ obiekt.obwod__gmina__okreg__id }}</li></a>

    html += '</ul>';
    $("#list_elements").html(html);
}

function getWojewodztwa(callback) {
    $.ajax({
        url: "http://localhost:8000/api/wybory/wojewodztwa/",
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            wyniki = result;
            localStorage.setItem("wojewodztwa", JSON.stringify(wyniki));
            callback();
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function renderLoadedData(data, edit) {
  
    if (edit) {
      fillResultTableEdit(data['wyniki_kandydatow']);
    } else {
      fillResultTable(data['wyniki_kandydatow']);
    }
    
    fillStatisticsTable(data['statystyki'], data['zakres']);
    fillListElements(data['lista_obiektow'], data['zakres']);

    drawChart(data['wyniki_kandydatow'], data['zakres'], data['statystyki']['id']);
}

function drawChart(wyniki_kandydatow, zakres, opis) {
    var nazwiska = [];
    var procenty = [];

    for (var key in wyniki_kandydatow) {
      if (wyniki_kandydatow.hasOwnProperty(key)) {
          nazwiska.push([wyniki_kandydatow[key]['nazwa']]);
          procenty.push([wyniki_kandydatow[key]['procent']]);
      }
    }

    var naglowek;
    if (zakres === 'kraj') {
        naglowek = 'Polska - wyniki wyborów';
    }
    else if (zakres === 'wojewodztwo') {
        naglowek = 'Województwo ' + opis + ' - wyniki wyborów';
    }
    else if (zakres === 'okreg') {
        naglowek = 'Okręg ' + opis + ' - wyniki wyborów';
    }
    else if (zakres === 'gmina') {
        naglowek = 'Gmina ' + opis + ' - wyniki wyborów';
    }
    else if (zakres === 'obwod') {
        naglowek = 'Obwód ' + opis + ' - wyniki wyborów';
    }

    var chart = {
      type: 'bar'
    };
    var title = {
      text: naglowek
    };
    var subtitle = {
      text: ''
    };
    var xAxis = {
        categories: nazwiska,
      title: {
         text: null
      }
   };
   var yAxis = {
      min: 0,
      title: {
         text: 'Procent oddanych glosow (%)',
         align: 'high'
      },
      labels: {
         overflow: 'justify'
      }
   };
   var tooltip = {
      valueSuffix: ' %'
   };
   var plotOptions = {
      bar: {
         dataLabels: {
            enabled: true
         }
      }
   };
   var legend = {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 100,
      floating: true,
      borderWidth: 1,
      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
      shadow: true
   };
   var credits = {
      enabled: false
   };

   var series= [{
            name: ' ',
            data: procenty,
        }];

   var json = {};
   json.chart = chart;
   json.title = title;
   json.subtitle = subtitle;
   json.tooltip = tooltip;
   json.xAxis = xAxis;
   json.yAxis = yAxis;
   json.series = series;
   json.plotOptions = plotOptions;
  // json.legend = legend;
   json.credits = credits;
   $('#container').highcharts(json);
}

function drawRegionsMap() {
    var wojewodztwa = localStorage.getItem('wojewodztwa');
    if (!wojewodztwa) {
        getWojewodztwa(drawRegionsMap);
        return;
    }

    wojewodztwa = JSON.parse(wojewodztwa);
    var arr = [['Województwo']];
    var dict = {};
    for (var key in wojewodztwa) {
      if (wojewodztwa.hasOwnProperty(key)) {
          arr.push([key]);
          dict[key] = window.location.pathname + "?wojewodztwo=" + key;
      }
    }
    //tutaj trzeba odpalic petle
    var data = google.visualization.arrayToDataTable(arr);

    //tutaj tez trzeba odpalic petle
    var links = dict;

    var options = {region: "PL", resolution: "provinces", legend: 'none',
      defaultColor: '#006699'
    };
    var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
    google.visualization.events.addListener(chart, 'select', function() {
        var sel = chart.getSelection();
        if (sel.length > 0) {
            var country = data.getValue(sel[0].row, 0);
            var link = links[country];
            if (link) {
                window.location.href = link;
            }
        }
    });
    chart.draw(data, options);
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function makeSearch() {
    window.location.href = 'search.html?gmina=' + document.forms["search_form"]["search_field"].value;
    return false;
}
google.charts.load('current', {'packages':['geochart']});
google.charts.setOnLoadCallback(drawRegionsMap);