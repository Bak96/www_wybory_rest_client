$(document).ready(init)
server_domain = "http://localhost:8000";
function init() {
    var zalogowany = localStorage.getItem('wybory_auth');
    zalogowany = JSON.parse(zalogowany);
    
    if (zalogowany == false) {
      $("#login_button").html('<a href="login.html">Login</a>');
    } else {
      $("#login_button").html('<a href="logout.html">Logout</a>');
    }
    parseURL();
}

function parseURL() {
    var search = window.location.search;

    if (!search) {
        return;
    }

    if (getParameterByName('gmina')) {
        par = getParameterByName('gmina');
        getData(server_domain + "/api/wybory/search?gmina=" + par, renderLoadedData);
    }
}

function getData(url, callback) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            wyniki = result;
            localStorage.setItem("search_data", JSON.stringify(wyniki));
            callback();
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function renderLoadedData() {
    var wyniki = localStorage.getItem('search_data');

    if (!wyniki) {
        console.error("brakuje danych");
        return;
    }
    wyniki = JSON.parse(wyniki);
    fillListElements(wyniki['lista_obiektow']);
}

function fillListElements(elements) {
    var query = '';

    if (!elements) {
        $("#list_label").hide();
        return;
    }

    $("#list_label").html('<hr><h3>Lista gmin:</h3>');
    query = 'gmina';

    var html = '<ul class="items">';
    var i;
    if(elements.length === 0) {
        $("#list_label").html('<h3>Brak wyników</h3>');
        $("#list_elements").hide();
        return;
    }
//<a href="/wybory/okreg/{{ obiekt.obwod__gmina__okreg__id }}"><li class="item">{{ obiekt.obwod__gmina__okreg__id }}</li></a>
    for (i = 0; i < elements.length; i++) {
        html += '<a href="wybory.html?' + query +'=' + elements[i]['id'] + '">' +
            '<li class="item">' + '(' +elements[i]['id'] + ')' + ' ' + elements[i]['nazwa_gminy'] +' </li></a>';
    }
    html += '</ul>';
    $("#list_elements").html(html);
}

function makeSearch() {
    window.location.href = 'search.html?gmina=' + document.forms["search_form"]["search_field"].value;
    return false;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}