
server_domain = "http://localhost:8000";


function getData(url, callback) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            wyniki = result;
            callback(wyniki['nazwa'], wyniki['glosy']);
        },
        error: function(error) {
            console.log(error);
        }
    });
}


function wybory_logout() {
    localStorage.setItem("wybory_auth", false);
    window.location.href = wybory.html;
}

function wybory_login() {
    var username = document.getElementById('login').value;
    var password = document.getElementById('password').value;

    url = server_domain + "/api/wybory/login/"
    data_to_send = {'username':username, 'password':password};

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(data_to_send),
        success: function(result) {
            if (result['auth']) {
                $("#error").html("");
                localStorage.setItem("wybory_auth", true);
                localStorage.setItem("wybory_username", username);
                localStorage.setItem("wybory_password", password);
                window.location.href = "wybory.html"
            } else {   
                localStorage.setItem("wybory_auth", false);
                $("#error").html("Wrong login or password");
            }
        },
        error: function(error) {
            console.log(error);
            alert("Error");
        }
    });


    return false;
}   