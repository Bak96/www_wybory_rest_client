$(document).ready(init)
server_domain = "http://localhost:8000";
function init() {
	var zalogowany = localStorage.getItem('wybory_auth');
    zalogowany = JSON.parse(zalogowany);
    
    if (zalogowany == false) {
      $("#login_button").html('<a href="login.html">Login</a>');
    } else {
      $("#login_button").html('<a href="logout.html">Logout</a>');
    }
    parseURL();
}

function parseURL() {
    var search = window.location.search;

    if (!search) {
        return;
    }

    if (getParameterByName('obwod')) {
        obwod = getParameterByName('obwod');
    } else {
    	return;
    }

    if (getParameterByName('kandydat')) {
    	kandydat = getParameterByName('kandydat');
    	
    } else {
    	return;
    }
    var url = server_domain + "/api/wybory/obwod/" + obwod + "/edit/kandydat/" + kandydat;
    getData(url, renderCandidateName);

}

function getData(url, callback) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            wyniki = result;
            callback(wyniki['nazwa'], wyniki['glosy']);
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function renderCandidateName(name, votes) {
	console.log(name);
	$("#nazwa_kandydata").html("&nbsp&nbsp" + name);
	document.getElementById('input_glosy').value = votes; 
}

function edit() {
	if (getParameterByName('obwod')) {
        obwod = getParameterByName('obwod');
        //getData(server_domain + "/api/wybory/search?gmina=" + par, renderLoadedData);
    } else {
    	return;
    }

    if (getParameterByName('kandydat')) {
    	kandydat = getParameterByName('kandydat');
    	
    } else {
    	return;
    }

    var url = server_domain + "/api/wybory/obwod/" + obwod + "/edit/kandydat/" + kandydat;
	var redirect_url = "wybory.html?obwod=" + obwod;
	var glosy = document.getElementById("input_glosy").value;

	var username = localStorage.getItem('wybory_username');
	var password = localStorage.getItem('wybory_password');

	data_to_send = {
		'glosy': glosy,
		'username': username,
		'password': password};

	sendData(url, data_to_send, redirect_url);
	return false;
}

function sendData(url, data_to_send, redirect_url) {
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'json',
		data: JSON.stringify(data_to_send),
		success: function(result) {
			console.log(result);
			alert("Successfully edited");
			window.location.href = redirect_url;
		},
		error: function(error) {
			console.log(error);
			alert("Cant edit data. Please log in");
		}
	});
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}