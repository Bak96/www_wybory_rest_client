#www_wybory_rest_client

Presentation of presidential election scores 2000 in Poland.  
Client uses javascript to get json data from restful server.
Client also can send requests to server. Page is generated on the fly.

Technology: javascript, ajax, html, css  

Link to the rest server: https://bitbucket.org/Bak96/www_wybory_rest  

![screenshot](https://bytebucket.org/Bak96/www_wybory_rest_client/raw/ebb256e3a4558ffebc02d1aa1d993d90228c64e7/screenshots/01.png)

![screenshot](https://bytebucket.org/Bak96/www_wybory_rest_client/raw/ebb256e3a4558ffebc02d1aa1d993d90228c64e7/screenshots/02.png)

![screenshot](https://bytebucket.org/Bak96/www_wybory_rest_client/raw/ebb256e3a4558ffebc02d1aa1d993d90228c64e7/screenshots/03.png)

![screenshot](https://bytebucket.org/Bak96/www_wybory_rest_client/raw/ebb256e3a4558ffebc02d1aa1d993d90228c64e7/screenshots/04.png)

